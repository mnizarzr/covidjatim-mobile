export const Messages = [
    "Jangan lupa cuci tangan", "Sering-sering cuci tangan", "Jaga kesehatan dan kebersihan",
    "#NengOmahWae", "Jaga jarak", "Rebahan di rumah untuk menyelamatkan dunia", "Jangan panik, tetap waspada! Jangan lebay, jangan abai!"
]

export const Color = {

    WHITE: "#FFF",
    BLACK: "#000",
    DARK: "#444",

    NEON_GREEN: "#81c784",
    LIGHT_GREEN: "#c8e6c9",
    WHATSAPP_BG: "#1EBEA5",

    RED: "#EC6666",
    DARK_RED: "#803737",

    BLUE: "#3EA2DC",
    DARK_BLUE: "#245E80",

    YELLOW: "#F2C94C",
    DARK_YELLOW: "#806A28"

}

export const Font = {
    LATO: {
        BLACK: "Lato-Black",
        BLACK_ITALIC: "Lato-BlackItalic",
        BOLD: "Lato-Bold",
        BOLD_ITALIC: "Lato-BoldItalic",
        ITALIC: "Lato-Italic",
        LIGHT: "Lato-Light",
        LIGHT_ITALIC: "Lato-LightItalic",
        REGULAR: "Lato-Regular",
        THIN: "Lato-Thin",
        THIN_ITALIC: "Lato-ThinItalic"
    },
    KARLA: {
        BOLD: "Karla-Bold",
        BOLD_ITALIC: "Karla-BoldItalic",
        ITALIC: "Karla-Italic",
        REGULAR: "Karla-Regular"
    }
}

export const ShadowStyle = {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
}

export const StorageKeys = {
    FAQ: "@CovidJatim:faq",
    LINK: "@CovidJatim:link"
}