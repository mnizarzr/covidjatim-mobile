import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import HomeScreen from './views/screens/HomeScreen'
import HospitalScreen from './views/screens/HospitalScreen';
import FaqScreen from './views/screens/FaqScreen';
import LinkScreen from './views/screens/LinkScreen'
import CaseScreen from './views/screens/CaseScreen';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Touchable from './views/components/PlatformTouchable'

import { Color, Font } from './assets/strings'

const BottomTabNavigator = () => {

    const Tab = createBottomTabNavigator()

    return (
        <Tab.Navigator
            initialRouteName="Home"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    switch (route.name) {
                        case "Home":
                            focused ? iconName = "home" : iconName = "home-outline"
                            break
                        case "Case":
                            focused ? iconName = "briefcase-search" : iconName = "briefcase-search-outline"
                            break
                        case "Hospital":
                            iconName = "hospital-building"
                            break
                        case "Faq":
                            focused ? iconName = "help-circle" : iconName = "help-circle-outline"
                            break
                        case "Link":
                            iconName = "link"
                            break
                    }

                    return <Icon name={iconName} size={size} color={color} style={{ marginTop: 6 }} />;
                },
                tabBarButton: props => <Touchable  {...props} background={Touchable.Ripple(Color.LIGHT_GREEN, true)} />
            })}
            tabBarOptions={{
                activeTintColor: Color.NEON_GREEN,
                inactiveTintColor: 'gray',
                labelStyle: { fontFamily: Font.LATO_REGULAR, marginBottom: 4 }
            }}
            backBehavior="initialRoute"
        >
            <Tab.Screen name="Home" component={HomeScreen} options={{ tabBarLabel: "Beranda" }} />
            <Tab.Screen name="Case" component={CaseScreen} options={{ tabBarLabel: "Kasus" }} />
            <Tab.Screen name="Hospital" component={HospitalScreen} options={{ tabBarLabel: "RS Rujukan" }} />
            <Tab.Screen name="Faq" component={FaqScreen} options={{ tabBarLabel: "FAQ" }} />
            <Tab.Screen name="Link" component={LinkScreen} options={{ tabBarLabel: "Pranala Luar" }} />
        </Tab.Navigator>
    )
}

export default function () {
    return (
        <NavigationContainer>
            <BottomTabNavigator />
        </NavigationContainer>
    )
}