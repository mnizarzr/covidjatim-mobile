import Action from './actionTypes'
import AsyncStorage from '@react-native-community/async-storage'
import { StorageKeys } from '../assets/strings'
import globalAxios from 'axios'

export const getFeed = (location) => {
    return async (dispatch, getState, { axios }) => {

        const getDistrict = axios.get(`/district/${location}`)
        const getGlobal = axios.get(`/global`)

        let dispatched = dispatch

        globalAxios.all([getDistrict, getGlobal])
            .then(globalAxios.spread(async (...responses) => {
                let district = await responses[0].data
                let global = await responses[1].data
                if (responses[0].status != 200 || responses[1].status != 200) {
                    dispatched({
                        type: Action.GET_FEED,
                        payload: responses[0].statusText || responses[1].statusText,
                        error: true
                    })
                } else {
                    dispatched({
                        type: Action.GET_FEED,
                        payload: Object.assign({}, district, global)
                    })
                }
            })).catch(error => {
                dispatched({
                    type: Action.GET_FEED,
                    payload: error,
                    error: true
                })
            })

    }
}

export const getCase = () => {
    return async (dispatch, getState, { axios }) => {

        const getDistrictsCase = axios.get(`/jatim`)
        const getProvincesCase = axios.get(`/provinces`)

        let dispatched = dispatch

        globalAxios.all([getDistrictsCase, getProvincesCase])
            .then(globalAxios.spread(async (...responses) => {
                if (responses[0].status != 200 || responses[1].status != 200) {
                    dispatched({
                        type: Action.GET_CASE,
                        payload: new Error(responses[0].statusText) || new Error(responses[1].statusText),
                        error: true
                    })
                } else {
                    dispatched({
                        type: Action.GET_CASE,
                        payload: [responses[0].data, responses[1].data.provinces]
                    })
                }
            })).catch(error => {
                dispatched({
                    type: Action.GET_CASE,
                    payload: error,
                    error: true
                })
            })

    }
}

export const getHospital = (location) => {
    return async (dispatch, getState, { axios }) => {
        await axios.get(`/hospitals/${location}`)
            .then(response => {
                if (response.status !== 200) {
                    dispatch({
                        type: Action.GET_HOSPITAL_BY_LOCATION,
                        payload: new Error(response.statusText),
                        error: true
                    })
                } else {
                    dispatch({
                        type: Action.GET_HOSPITAL_BY_LOCATION,
                        payload: response.data
                    })
                }
            })
            .catch(error => {
                dispatch({
                    type: Action.GET_HOSPITAL_BY_LOCATION,
                    payload: error,
                    error: true
                })
            })
    }
}

export const getFaq = () => {
    return async (dispatch, getState, { axios }) => {
        let faqs = await AsyncStorage.getItem(StorageKeys.FAQ)
        if (faqs != null) {
            dispatch({
                type: Action.GET_FAQ,
                payload: JSON.parse(faqs)
            })
        } else {
            await axios.get(`/faq`)
                .then(async (response) => {
                    if (response.status !== 200) {
                        dispatch({
                            type: Action.GET_FAQ,
                            payload: new Error(response.statusText),
                            error: true
                        })
                    } else {

                        await AsyncStorage.setItem(StorageKeys.FAQ, JSON.stringify(response.data))

                        dispatch({
                            type: Action.GET_FAQ,
                            payload: response.data
                        })
                    }
                })
                .catch(error => {
                    dispatch({
                        type: Action.GET_FAQ,
                        payload: error,
                        error: true
                    })
                })
        }
    }
}

export const getLink = () => {
    return async (dispatch, getState, { axios }) => {
        let links = await AsyncStorage.getItem(StorageKeys.LINK)
        if (links != null) {
            dispatch({
                type: Action.GET_FAQ,
                payload: JSON.parse(links)
            })
        } await axios.get(`/links`)
            .then(async (response) => {
                if (response.status !== 200) {
                    dispatch({
                        type: Action.GET_LINKS,
                        payload: new Error(response.statusText),
                        error: true
                    })
                } else {
                    await AsyncStorage.setItem(StorageKeys.LINK, JSON.stringify(response.data))
                    dispatch({
                        type: Action.GET_LINKS,
                        payload: response.data
                    })
                }
            })
            .catch(error => {
                dispatch({
                    type: Action.GET_LINKS,
                    payload: error,
                    error: true
                })
            })
    }
}