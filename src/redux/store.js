import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import axios from 'axios'
import reducers from './reducers'
import logger from 'redux-logger'

const BASE_URL = `http://covidjawatimur-env.eba-mn3aatsh.us-east-1.elasticbeanstalk.com`

const axiosInstance = axios.create({ baseURL: BASE_URL })

let middlewares = [thunk.withExtraArgument({ axios: axiosInstance })]

if (__DEV__) {
    axiosInstance.interceptors.request.use((config) => {
        console.log(`${config.method} => ${config.baseURL}${config.url}`)
        return config
    })

    middlewares.push(logger)
}

export default createStore(reducers, applyMiddleware(...middlewares));