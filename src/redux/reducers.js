import { combineReducers } from 'redux'
import Action from './actionTypes'

const feedInitialState = {
    loading: true,
    payload: {
        province: {
            id: 0,
            province_id: 0,
            province_name: "",
            confirmed: 0,
            recovered: 0,
            deaths: 0
        },
        district: {
            district_name: "",
            odr: "",
            otg: "",
            odp: "",
            pdp: "",
            confirmed: "",
            updated_at: "",
            district_id: ""
        }
    }
}

const feed = (state = feedInitialState, { type, payload, error }) => {
    switch (type) {
        case Action.GET_FEED:
            return Object.assign({}, state, error, { loading: false, payload })
        default:
            return state
    }
}

const casesInitialState = {
    loading: true,
    payload: []
}

const cases = (state = casesInitialState, { type, payload, error }) => {
    switch (type) {
        case Action.GET_CASE:
            return Object.assign({}, state, error, { loading: false, payload })
        default:
            return state
    }
}

const hospital = (state = { loading: true }, { type, payload, error }) => {
    switch (type) {
        case Action.GET_HOSPITAL_BY_LOCATION:
            return Object.assign({}, state, error, {
                loading: false,
                hospitals: [
                    { title: "RS Rujukan Utama", data: payload.primary },
                    { title: "RS Rujukan di Daerahmu", data: payload.nearest }
                ]
            })
        default:
            return state
    }
}

const faq = (state = { loading: true }, { type, payload, error }) => {
    switch (type) {
        case Action.GET_FAQ:
            return Object.assign({}, state, error, { loading: false, faq: payload })
        default:
            return state
    }
}

const link = (state = { loading: true, link: [] }, { type, payload, error }) => {
    switch (type) {
        case Action.GET_LINKS:
            return Object.assign({}, state, error, {
                loading: false, link: [
                    {
                        title: "Donasi",
                        data: payload.filter(element => element.category === "Donation")
                    },
                    {
                        title: "Informasi",
                        data: payload.filter(element => element.category === "Information")
                    },
                    {
                        title: "Pemerintah",
                        data: payload.filter(element => element.category === "Government")
                    }
                ]
            })
        default:
            return state
    }
}


export default combineReducers({ feed, cases, hospital, faq, link })