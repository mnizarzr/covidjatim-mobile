import * as React from 'react';
import { SafeAreaView, View, Text, ActivityIndicator, ScrollView, TouchableWithoutFeedback, Dimensions, TextInput } from 'react-native'
import moment from 'moment'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../redux/actions'
import Accordion from 'react-native-collapsible/Accordion'
import { Color, ShadowStyle, Font } from '../../assets/strings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const { width, height } = Dimensions.get('window')

class FaqScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            faq: [],
            activeSections: []
        }

        this.searchText = this._searchText.bind(this)

    }

    async componentDidMount() {
        await this.props.getFaq()
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.faq != prevProps.faq) {
            this.setState((state, props) => ({
                faq: props.faq
            }));
        }
    }

    _renderContent = section => {
        let parseText = section.answer.replace(/<ul>/gi, '\u2022')
        return (
            <View style={{ marginTop: 12 }} >
                <Text style={{ fontFamily: Font.LATO.REGULAR }} >{parseText}</Text>
            </View>
        );
    };

    _searchText = (text) => {
        this.setState((state, props) => ({
            faq: props.faq.filter(el => el.question.toLowerCase().includes(text.toLowerCase()))
        }))
    }

    _updateSections = activeSections => {
        this.setState({ activeSections });
    };

    render() {
        return <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
            {
                this.props.loading
                    ?
                    <ActivityIndicator style={{ flex: 1 }} color={Color.NEON_GREEN} size="large" />
                    :
                    <View >
                        <View style={[ShadowStyle, {
                            alignSelf: 'center', width: width - 24, borderRadius: 50, height: 36, backgroundColor: 'white',
                            marginTop: 12, paddingHorizontal: 6, justifyContent: 'center', alignItems: 'center', flexDirection: 'row'
                        }]} >
                            <Icon name="magnify" color="grey" size={24} />
                            <TextInput style={{ fontFamily: Font.LATO.REGULAR, flex: 1, color: 'black', justifyContent: 'center' }}
                                textAlignVertical="center"
                                placeholder="Apa itu virus corona...?"
                                placeholderTextColor="grey"
                                onChangeText={(text) => this.searchText(text)} />
                        </View>
                        <ScrollView  >
                            <Accordion
                                containerStyle={{ padding: 12 }}
                                sections={this.state.faq}
                                activeSections={this.state.activeSections}
                                renderHeader={(section) => <Text style={{ fontFamily: Font.LATO.BOLD }} >{section.question}</Text>}
                                renderContent={this._renderContent}
                                onChange={this._updateSections}
                                touchableComponent={TouchableWithoutFeedback}
                                sectionContainerStyle={[ShadowStyle, { backgroundColor: 'white', borderRadius: 12, padding: 12, marginBottom: 6 }]}
                            />
                        </ScrollView>
                    </View>
            }

        </SafeAreaView>
    }

}

function mapStateToProps(state, props) {
    return state.faq
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(FaqScreen)