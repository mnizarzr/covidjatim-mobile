import * as React from 'react';
import { SafeAreaView, View, Text, SectionList, Linking, Dimensions, ActivityIndicator } from 'react-native'
import moment from 'moment'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../redux/actions'
import Touchable from '../components/PlatformTouchable'
import { Color, Font, ShadowStyle } from '../../assets/strings'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const { width, height } = Dimensions.get('window')
class LinkScreen extends React.Component {

    constructor(props) {
        super(props)

    }

    async componentDidMount() {
        this.props.getLink()
    }

    _onPressItem = async (url) => {

        await Linking.canOpenURL(url)
            .then(async (res) => {
                if (res) await Linking.openURL(url)
            })

    }

    render() {
        return <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
            {
                this.props.loading && this.props.link.length == 0 ?
                    <ActivityIndicator style={{ flex: 1 }} color={Color.NEON_GREEN} size="large" />
                    :
                    <SectionList
                        renderSectionHeader={({ section: { title } }) => <Text style={{ fontFamily: Font.LATO.BOLD, marginVertical: 12, fontSize: 24 }} children={title} />}
                        contentContainerStyle={{ padding: 12 }}
                        sections={this.props.link}
                        renderItem={({ item, index }) => {
                            return <>
                                <View style={[ShadowStyle, { width: width - 24, marginBottom: 6, backgroundColor: 'white', borderRadius: 12 }]} >
                                    <Touchable style={{ borderRadius: 12, flex: 1, padding: 12, }} onPress={() => this._onPressItem(item.url)} >
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                            <Text style={{ width: width - 78, fontFamily: Font.LATO.REGULAR, fontSize: 18 }} numberOfLines={2} children={item.name} />
                                            <Icon name="launch" color="black" size={24} />
                                        </View>
                                    </Touchable>
                                </View>
                            </>
                        }
                        }
                        keyExtractor={(item, index) => String(index)} />
            }
        </SafeAreaView >
    }

}

function mapStateToProps(state, props) {
    return state.link
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LinkScreen)