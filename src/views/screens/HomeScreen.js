import * as React from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Linking, Dimensions, SafeAreaView, ScrollView, RefreshControl, StatusBar, PermissionsAndroid, ToastAndroid } from 'react-native'
import Touchable from '../components/PlatformTouchable'
import { Color, Font, Messages, ShadowStyle } from '../../assets/strings'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import moment from 'moment'
import { bindActionCreators } from 'redux';
import * as actions from '../../redux/actions';
import Geolocation from 'react-native-geolocation-service'
import { connect } from 'react-redux';
import { sentenceCase } from '../../helpers/StringHelper'

const { width, height } = Dimensions.get('window')

class HomeScreen extends React.Component {

    constructor(props) {
        super(props)


        this.state = {
            refreshing: false,
            location: ""
        }

        this.onRefresh = this._onRefresh.bind(this)

    }

    async componentDidMount() {

        const request = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: "Location Permission",
                message:
                    `Aplikasi membutuhkan izin lokasi untuk mendapatkan data rumah sakit di daerah anda`,
                buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK"
            }
        )
        if (request === PermissionsAndroid.RESULTS.GRANTED) {
            Geolocation.getCurrentPosition(
                async (position) => {
                    const { latitude, longitude } = position.coords
                    this.setState({
                        location: `${latitude},${longitude}`
                    })
                    this.state.location != "" ? await this.props.getFeed(this.state.location) : undefined
                },
                (error) => {
                    ToastAndroid.show(`${error.code}: ${error.message}`, ToastAndroid.SHORT)
                },
                { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
            )
        } else {
            console.log("Location permission denied");
        }

    }

    async _onRefresh() {
        this.setState((prevState) => {
            return { refreshing: !prevState.refreshing }
        })
        this.state.location != "" ? await this.props.getFeed(this.state.location) : undefined
        this.setState((prevState) => {
            return { refreshing: !prevState.refreshing }
        })
    }

    render() {

        const waCovid = "https://api.whatsapp.com/send?phone=6282133918782&text=Untuk%20mengetahui%20Info%20COVID-19%20di%20Jawa%20Timur%20Silahkan%20Klik-%3E"

        return <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar backgroundColor={Color.NEON_GREEN} />

            {this.props.error ? ToastAndroid.show(this.props.payload.message, ToastAndroid.SHORT) : undefined}

            <ScrollView style={{ flex: 1, padding: 12 }} showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.onRefresh()} />
                } >

                <View style={{ marginBottom: 12 }} >
                    <Text
                        style={{ fontSize: 24, fontFamily: Font.LATO.BOLD }}
                        children={`Selamat ${moment().format('a')}`}
                    />
                    <Text
                        style={{ marginTop: 8, fontSize: 14, fontFamily: Font.LATO.REGULAR }}
                        children={Messages[Math.floor(Math.random() * Messages.length)]}
                    />
                </View>

                <View style={{ marginVertical: 12 }} >
                    <Text style={[styles.heading, { marginBottom: 12 }]} children="Nomor Penting" />

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 6 }}>
                        <View style={[ShadowStyle, styles.cardPhone, { marginRight: 3 }]}>
                            <Touchable
                                style={{ padding: 12, borderRadius: 12 }}
                                onPress={async () => { await Linking.openURL("tel:119") }}>
                                <View>
                                    <Text style={styles.phoneTitle} children="Covid-19 Hotline" />
                                    <Text style={styles.phoneNumber} children="119" />
                                </View>
                            </Touchable>
                        </View>

                        <View style={[ShadowStyle, styles.cardPhone, { marginLeft: 3 }]}>
                            <Touchable
                                style={{ padding: 12, borderRadius: 12 }}
                                onPress={async () => { await Linking.openURL("tel:1500117") }}>
                                <View>
                                    <Text style={styles.phoneTitle} children="Call Center" />
                                    <Text style={styles.phoneNumber} children="1500 117" />
                                </View>
                            </Touchable>
                        </View>
                    </View>

                    <Touchable
                        style={[ShadowStyle, { width: 'auto', height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: Color.WHATSAPP_BG, padding: 12, borderRadius: 12 }]}
                        onPress={async () => await Linking.openURL(waCovid)}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name="whatsapp" color="white" size={30} style={{ marginRight: 6 }} />
                            <Text style={{ fontFamily: Font.LATO.REGULAR, fontSize: 18, color: 'white' }} children="WA Radar Covid Jatim" />
                        </View>
                    </Touchable>
                </View>

                {this.props.loading || this.props.payload.province == undefined || this.props.payload.district == undefined
                    ? <ActivityIndicator size="large" color={Color.NEON_GREEN} style={{ flex: 1 }} />
                    :

                    <View>
                        <View style={{ marginVertical: 12 }} >
                            <Text style={[styles.heading, { marginBottom: 3 }]} children={`Situasi di ${sentenceCase(this.props.payload.district.district_name)}`} />
                            <Text style={[styles.infoText, { marginBottom: 6 }]} children={`Terakhir diperbarui: ${moment(this.props.payload.district.updated_at).format('dddd, DD MMMM YYYY HH:mm')}`} />
                            <View style={{ width: width - 24, flexDirection: 'row', justifyContent: 'space-between' }} >
                                <View style={[ShadowStyle, styles.districtStats, { padding: 12, backgroundColor: Color.RED }]} >
                                    <Text style={[styles.statsTitle, { color: Color.DARK_RED }]} children="Dirawat" />
                                    <Text style={{ fontFamily: Font.LATO.REGULAR, fontSize: 30, color: 'white' }}>{this.props.payload.district.treated}</Text>
                                </View>
                                <View style={[ShadowStyle, styles.districtStats, { padding: 12, backgroundColor: Color.BLUE }]} >
                                    <Text style={[styles.statsTitle, { color: Color.DARK_BLUE }]} children="Sembuh" />
                                    <Text style={{ fontFamily: Font.LATO.REGULAR, fontSize: 30, color: 'white' }}>{this.props.payload.district.recovered}</Text>
                                </View>
                                <View style={[ShadowStyle, styles.districtStats, { padding: 12, backgroundColor: Color.YELLOW }]} >
                                    <Text style={[styles.statsTitle, { color: Color.DARK_YELLOW }]} children="Meninggal" />
                                    <Text style={{ fontFamily: Font.LATO.REGULAR, fontSize: 30, color: 'white' }}>{this.props.payload.district.deaths}</Text>
                                </View>
                            </View>
                            <Text style={{ fontFamily: Font.LATO.ITALIC, fontSize: 10, marginTop: 6 }} children="Sumber data: Info Covid-19 Jatim" />
                        </View>

                        <View style={{ marginVertical: 12, marginBottom: 18 }}  >
                            <Text style={[styles.heading, { marginBottom: 3 }]} children="Situasi di Jawa Timur" />
                            <Text style={[styles.infoText, { marginBottom: 6 }]} children={`Terakhir diperbarui: ${moment(this.props.payload.updated).format('dddd, DD MMMM YYYY HH:mm')}`} />

                            <View style={[ShadowStyle, styles.caseStats, { backgroundColor: Color.RED, marginBottom: 6 }]} >
                                <Text style={[styles.statsTitle, { color: Color.DARK_RED }]} children="Terkonfirmasi" />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 6 }} >
                                    <Text style={styles.casePrimary} children="Jawa Timur" />
                                    <Text style={styles.casePrimary} children={this.props.payload.province.confirmed} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3 }} >
                                    <Text style={styles.caseSecondary} children="Indonesia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.cases} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={styles.caseSecondary} children="Dunia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.global.cases} />
                                </View>
                            </View>

                            <View style={[ShadowStyle, styles.caseStats, { backgroundColor: Color.BLUE, marginBottom: 6 }]} >
                                <Text style={[styles.statsTitle, { color: Color.DARK_BLUE }]} children="Sembuh" />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 6 }} >
                                    <Text style={styles.casePrimary} children="Jawa Timur" />
                                    <Text style={styles.casePrimary} children={this.props.payload.province.recovered} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3 }} >
                                    <Text style={styles.caseSecondary} children="Indonesia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.recovered} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={styles.caseSecondary} children="Dunia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.global.recovered} />
                                </View>
                            </View>

                            <View style={[ShadowStyle, styles.caseStats, { backgroundColor: Color.YELLOW }]} >
                                <Text style={[styles.statsTitle, { color: Color.DARK_YELLOW }]} children="Meninggal" />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 6 }} >
                                    <Text style={styles.casePrimary} children="Jawa Timur" />
                                    <Text style={styles.casePrimary} children={this.props.payload.province.deaths} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3 }} >
                                    <Text style={styles.caseSecondary} children="Indonesia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.deaths} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={styles.caseSecondary} children="Dunia" />
                                    <Text style={styles.caseSecondary} children={this.props.payload.global.deaths} />
                                </View>
                            </View>

                            <Text style={[styles.infoText, { marginTop: 6 }]} children="Sumber data: Worldometers & BNPB Covid-19" />
                        </View>
                    </View>
                }
            </ScrollView>

        </SafeAreaView >
    }

}

const styles = StyleSheet.create({
    heading: {
        fontFamily: Font.LATO.BOLD,
        fontSize: 24
    },
    cardPhone: {
        flex: 1,
        backgroundColor: Color.NEON_GREEN,
        borderRadius: 12,
        // padding: 12
    },
    phoneTitle: {
        fontFamily: Font.LATO.REGULAR,
        fontSize: 16,
        alignSelf: 'center',
        marginBottom: 8
    },
    phoneNumber: {
        fontFamily: Font.KARLA.BOLD,
        fontSize: 30,
        alignSelf: 'center',
    },
    statsTitle: {
        textAlign: 'center',
        fontFamily: Font.LATO.BOLD,
        fontSize: 18
    },
    districtStats: {
        width: width * 1 / 3 - 12,
        borderRadius: 12,
        alignItems: 'center'
    },
    caseStats: {
        width: width - 24,
        padding: 12,
        borderRadius: 12
    },
    casePrimary: {
        fontSize: 20,
        fontFamily: Font.LATO.BOLD
    },
    caseSecondary: {
        fontSize: 12,
        fontFamily: Font.LATO.REGULAR,
        color: 'white'
    },
    infoText: {
        fontFamily: Font.KARLA.ITALIC,
        fontSize: 10
    }
})

function mapStateToProps(state, props) {
    return state.feed
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)