import * as React from 'react';
import { View, Text, SafeAreaView, SectionList, ActivityIndicator, PermissionsAndroid, ToastAndroid } from 'react-native'
import moment from 'moment'
import HospitalCard from '../components/HospitalCard';
import { Font, Color } from '../../assets/strings';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';
import Geolocation from 'react-native-geolocation-service'

class HospitalScreen extends React.Component {

    constructor(props) {
        super(props)

        this._intervalGetHospital;

    }

    async componentDidMount() {

        this._unsubscribeFocusListener = this.props.navigation.addListener('focus', async () => {
            const checkPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if (checkPermission) {

                Geolocation.getCurrentPosition(
                    (position) => {
                        const { latitude, longitude } = position.coords

                        this.props.getHospital(`${latitude},${longitude}`)

                        // will get user location every minute so that
                        // if user is moving their distance to nearest hospitals
                        // will be updated
                        this._intervalGetHospital = setInterval(() => {
                            this.props.getHospital(`${latitude},${longitude}`)
                        }, 60000)
                    },
                    (error) => {
                        ToastAndroid.show(`${error.code}: ${error.message}`, ToastAndroid.SHORT)
                    },
                    { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
                )
            } else {
                console.log("Location permission denied");
            }
        })

        this._unsubscribeBlurListener = this.props.navigation.addListener('blur', () => {
            clearInterval(this._intervalGetHospital)
        })


    }

    componentWillUnmount() {
        this._unsubscribeFocusListener()
        this._unsubscribeBlurListener()
    }

    render() {
        return <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >

            {
                this.props.loading
                    ?
                    <ActivityIndicator size="large" color={Color.NEON_GREEN} style={{ flex: 1 }} />
                    :
                    <SectionList
                        renderSectionHeader={({ section: { title, data } }) => {
                            return <>
                                {data != null && data.length > 0
                                    ?
                                    <Text style={{ fontFamily: Font.LATO.BOLD, marginVertical: 12, fontSize: 24 }} children={title} />
                                    :
                                    undefined
                                }
                            </>
                        }}
                        contentContainerStyle={{ padding: 12 }}
                        sections={this.props.hospitals}
                        renderItem={({ item, index }) => <HospitalCard index={index} item={item} />}
                        keyExtractor={(item, index) => index.toString()} />

            }

        </SafeAreaView>

    }

}

function mapStateToProps(state, props) {
    return state.hospital
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HospitalScreen)