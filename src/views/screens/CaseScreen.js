import * as React from 'react'
import { SafeAreaView, View, FlatList, TextInput, Dimensions, ToastAndroid, ActivityIndicator } from 'react-native'
import { bindActionCreators } from 'redux'
import * as actions from '../../redux/actions'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { ShadowStyle, Font, Color } from '../../assets/strings'
import CaseCard from '../components/CaseCard'

const { width, height } = Dimensions.get('window')

class CaseScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            data: []
        }

        this.onSearch = this._onSearchChange.bind(this)

    }

    async componentDidMount() {

        await this.props.getCase()
        if (this.props.error) ToastAndroid.show(this.props.payload.message, ToastAndroid.SHORT)

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.payload.length != prevProps.payload.length) {
            this.setState((state, props) => ({
                data: props.payload[0]
            }));
        }
    }

    _onSearchChange = (text) => {

        text = text.toLowerCase()

        let defaultData = this.props.payload[0]
        let districtData = this.props.payload[0].filter(el => el.district_name.toLowerCase().includes(text))
        let provinceData = this.props.payload[1].filter(el => el.province_name.toLowerCase().includes(text))

        let newData
        text.trim() == "" ? newData = defaultData : newData = districtData.concat(provinceData)

        this.setState({
            data: newData
        })

    }

    render() {
        return <>
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
                <View style={[ShadowStyle, {
                    alignSelf: 'center', width: width - 24, borderRadius: 50, height: 36, backgroundColor: 'white',
                    marginTop: 12, paddingHorizontal: 6, justifyContent: 'center', alignItems: 'center', flexDirection: 'row'
                }]} >
                    <Icon name="magnify" color="grey" size={24} />
                    <TextInput style={{ fontFamily: Font.LATO.REGULAR, flex: 1, color: 'black', justifyContent: 'center' }}
                        textAlignVertical="center"
                        placeholder="Cari kota / kabupaten / provinsi..."
                        placeholderTextColor="grey"
                        onChangeText={(text) => this.onSearch(text)} />
                </View>

                {this.props.loading
                    ?
                    <ActivityIndicator size="large" color={Color.NEON_GREEN} style={{ flex: 1 }} />
                    :
                    <FlatList
                        style={{ flex: 1 }}
                        contentContainerStyle={{ alignSelf: 'center', marginTop: 12 }}
                        data={this.state.data}
                        extraData={this.state.data}
                        renderItem={({ item, index }) => <CaseCard index={index} item={item} />}
                        keyExtractor={(item, index) => String(item.district_id || item.province_id)}
                    />
                }

            </SafeAreaView>
        </>
    }

}

function mapStateToProps(state, props) {
    return state.cases
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CaseScreen)