import * as React from 'react'
import { View, Dimensions, Text, StyleSheet } from 'react-native'
import { ShadowStyle, Font } from '../../assets/strings'
import moment from 'moment'

const { width, height } = Dimensions.get('window')

const CaseCard = ({ index, item }) => {

    const ifProvince = item.province_id != undefined ? true : false

    return <>

        <View style={[ShadowStyle, { width: width - 22, marginHorizontal: 2, padding: 12, borderRadius: 12, backgroundColor: 'white', marginBottom: 12 }]} >
            <Text style={{ fontFamily: Font.LATO.BOLD, fontSize: 18, color: 'black', marginBottom: 8 }} >{item.district_name || item.province_name}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                <Text style={styles.itemText} >Terkonfirmasi</Text>
                <Text style={styles.itemText} >{item.confirmed || item.total}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                <Text style={styles.itemText} >Sembuh</Text>
                <Text style={styles.itemText} >{item.recovered}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                <Text style={styles.itemText} >Meninggal</Text>
                <Text style={styles.itemText} >{item.deaths}</Text>
            </View>
            {ifProvince ? <View /> : <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                    <Text style={styles.itemText} >Dirawat</Text>
                    <Text style={styles.itemText} >{item.treated}</Text>
                </View>
                <Text style={styles.itemText} >Diperbarui pada: {moment(item.updated_at).format("DD MMMM YYYY HH:mm")}</Text>
            </View>}
        </View>

    </>

}

const styles = StyleSheet.create({
    itemText: {
        fontFamily: Font.LATO.REGULAR,
        fontSize: 14,
        marginBottom: 6
    }
})

export default CaseCard