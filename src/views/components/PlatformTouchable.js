/*
Copyright (c) 2017, React Native Platform Touchable Contributors
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import React from 'react';
import {
    Platform,
    TouchableNativeFeedback,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';

let TouchableComponent;

if (Platform.OS === 'android') {
    TouchableComponent =
        Platform.Version <= 20 ? TouchableOpacity : TouchableNativeFeedback;
} else {
    TouchableComponent = TouchableOpacity;
}

if (TouchableComponent !== TouchableNativeFeedback) {
    TouchableComponent.SelectableBackground = () => ({});
    TouchableComponent.SelectableBackgroundBorderless = () => ({});
    TouchableComponent.Ripple = () => ({});
    TouchableComponent.canUseNativeForeground = () => false;
}

export default class PlatformTouchable extends React.Component {
    static SelectableBackground = TouchableComponent.SelectableBackground;
    static SelectableBackgroundBorderless = TouchableComponent.SelectableBackgroundBorderless;
    static Ripple = TouchableComponent.Ripple;
    static canUseNativeForeground = TouchableComponent.canUseNativeForeground;

    render() {
        let {
            children,
            style,
            foreground,
            background,
            useForeground,
            ...props
        } = this.props;

        // Even though it works for TouchableWithoutFeedback and
        // TouchableNativeFeedback with this component, we want
        // the API to be the same for all components so we require
        // exactly one direct child for every touchable type.
        children = React.Children.only(children);

        if (TouchableComponent === TouchableNativeFeedback) {
            useForeground =
                foreground && TouchableNativeFeedback.canUseNativeForeground();

            if (foreground && background) {
                console.warn(
                    'Specified foreground and background for Touchable, only one can be used at a time. Defaulted to foreground.'
                );
            }

            return (
                <TouchableComponent
                    {...props}
                    useForeground={useForeground}
                    background={(useForeground && foreground) || background}>
                    <View style={style}>
                        {children}
                    </View>
                </TouchableComponent>
            );
        } else if (TouchableComponent === TouchableWithoutFeedback) {
            return (
                <TouchableWithoutFeedback {...props}>
                    <View style={style}>
                        {children}
                    </View>
                </TouchableWithoutFeedback>
            );
        } else {
            let TouchableFallback = this.props.fallback || TouchableComponent;
            return (
                <TouchableFallback {...props} style={style}>
                    {children}
                </TouchableFallback>
            );
        }
    }
}