import * as React from 'react'
import { View, Text, Linking, ToastAndroid, StyleSheet } from 'react-native'
import Touchable from '../components/PlatformTouchable'
import { Font, Color, ShadowStyle } from '../../assets/strings'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { AsYouType } from 'libphonenumber-js'

const HospitalCard = ({ index, item }) => {

    const onPressPhone = async () => {

        const uri = `tel:${item.phone}`

        await Linking.canOpenURL(uri)
            .then(async (val) => {
                if (val) await Linking.openURL(uri)
            })

    }

    const onPressLocation = async () => {

        const location = item.latitude.toString().concat(",", item.longitude.toString())
        const uri = `https://maps.google.com/?daddr=${location}`

        await Linking.canOpenURL(uri)
            .then(async (val) => {
                if (val) await Linking.openURL(uri)
                else {
                    ToastAndroid.show("Tidak bisa membuka maps", ToastAndroid.SHORT)
                }
            })

    }

    return <>
        <View style={[ShadowStyle, {
            backgroundColor: 'white',
            padding: 12,
            borderRadius: 12,
            marginTop: index == 0 ? 0 : 6,
            marginBottom: 6
        }]}>

            <Text style={{
                fontSize: 18,
                fontFamily: Font.LATO.BOLD,
            }} children={item.name} />

            <Text style={{
                fontSize: 14,
                fontFamily: Font.LATO.LIGHT,
                marginVertical: 6
            }} children={item.address} />

            <Text style={{
                fontSize: 14,
                fontFamily: Font.LATO.LIGHT
            }} children={`${item.distance_km} KM`} />

            <View style={{ flexDirection: 'row', justifyContent: "space-between", marginVertical: 6 }} >
                {item.phone === null ? <View /> :
                    <Touchable style={{ flex: 1, backgroundColor: "#4CAF50", padding: 6, marginRight: 3, borderRadius: 8 }} onPress={onPressPhone} background={Touchable.Ripple('white')} >
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <Icon style={{ marginRight: 6 }} name="phone" color="white" size={24} />
                            <Text style={{ fontFamily: Font.LATO.REGULAR, color: 'white' }} children={new AsYouType("ID").input(item.phone)} />
                        </View>
                    </Touchable>}

                <Touchable style={{ flex: 1, backgroundColor: "#2196F3", padding: 6, marginLeft: item.phone === null ? 0 : 3, borderRadius: 8 }} onPress={onPressLocation} background={Touchable.Ripple('white')} >
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                        <Icon style={{ marginRight: 6 }} name="navigation" color="white" size={24} />
                        <Text style={{ fontFamily: Font.LATO.REGULAR, color: 'white' }} children="Navigasi" />
                    </View>
                </Touchable>
            </View>

            <View style={{ height: 3, borderRadius: 4, backgroundColor: Color.LIGHT_GREEN }} />

        </View>
    </>

}

const style = StyleSheet.create({

})

export default HospitalCard