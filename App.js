import * as React from 'react';
import { Provider } from 'react-redux';
import NavigationContainer from './src/Navigator'
import store from './src/redux/store'

const App = () => {
    return <>
        <Provider store={store} >
            <NavigationContainer />
        </Provider>
    </>
}

export default App;
